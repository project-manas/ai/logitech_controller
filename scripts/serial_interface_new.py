#!/usr/bin/env python
import rospy
import serial
from original_logitech.msg import carDriveMsg
from geometry_msgs.msg import Twist

rospy.init_node('eve_serial_comm', anonymous=True, log_level=rospy.WARN)
readFrequency = 150

# dict={'R':-1,'N':0,'D':1,'B':2}

if rospy.has_param('~serial_port'):
	serial_port = rospy.get_param('~serial_port')
else:
	serial_port = '/dev/ttyACM0'

if rospy.has_param('~baudrate'):
	baudrate = rospy.get_param('~baudrate')

else:
	baudrate = 115200

if rospy.has_param("~enableLogitech"):
	enableLogitech = rospy.get_param('~enableLogitech')  # TODO:

ser = serial.Serial(serial_port, baudrate, timeout=1)
ser.flushInput()
rate = rospy.Rate(readFrequency)
command = ""
sign = lambda x: (2, 1)[x < 0]
gear_map = {'-1': 1, '0': 2, '1': 4, 'B': 16}
indicator_map = {'O': 0, 'L': 8, 'R': 1, 'H': 9}


def cmd_callback(msg):
	accelValue = 't' + str(calibrate_accel(msg.accel))
	brakeValue = 'b' + str(calibrate_brake(msg.brake))
	steerValue = 's' + str(int((msg.steering) * (-35 / 32767.0)))
	gearValue = 'g' + str(msg.gear)
	ser.write(parse_cmd(accelValue))
	ser.write(parse_cmd(brakeValue))
	ser.write(parse_cmd(steerValue))
	ser.write(parse_cmd(gearValue))


def calibrate_accel(data):
	if data >= 0:
		return (32767 - data) * 100 / 65534
	return int((32767 - data) * 100 / 65534)


def calibrate_brake(data):
	result = int((32767 - data) * 100 / 65534)
	return int(min(result * 150, 100))


'''parse functions'''


def gear_parse(cmd):
	l = bytearray([127, 103, 0, 0, 126])
	gear_val = ((gear_map[cmd[1:]] << 3) + 4)
	l[3] = gear_val % 128
	l[2] = gear_val / 128
	return l


def indicator_parse(cmd):
	l = bytearray([127, 105, 0, 0, 126])
	indicator_val = ((indicator_map[cmd[1]] << 4) + 8) or ((int(cmd[3]) << 2) + 1)
	l[3] = indicator_val % 128
	l[2] = indicator_val / 128
	return l


def steering_parse(cmd):
	l = bytearray([127, 115, 0, 0, 126])
	steer_val = int(cmd[1:])
	print "Steering Value(angle) : ", steer_val
	# if abs(prev_steer_val) == 1 and steer_val == 0:
	# 	steer_val = sign(prev_steer_val)
	# elif abs(prev_steer_val) == 0 and abs(steer_val) == 1:
	# 	steer_val = sign(steer_val)
	# else:
	# 	steer_val =(abs(steer_val)<<2) + sign(steer_val)
	steer_val = (abs(steer_val) << 2) + sign(steer_val)

	print "Steering Value(Mapped) : ", steer_val
	# print "Steering angle feedback: ",a[1],'\n'
	print "\n\n"
	l[3] = steer_val % 127
	l[2] = steer_val / 128
	return l


def braking_parse(cmd):
	l = bytearray([127, 98, 0, 0, 126])
	brake_val = int(cmd[1:])
	brake_val = (brake_val << 1) + 1
	l[3] = brake_val % 127
	l[2] = brake_val / 128
	return l


def throttle_parse(cmd):
	l = bytearray([127, 116, 0, 0, 126])
	throttle_val = int(cmd[1:])
	throttle_val = (throttle_val << 1) + 1
	l[3] = throttle_val % 127
	l[2] = throttle_val / 128
	return l


def wiperHorn_parse(cmd):
	l = bytearray([127, 119, 0, 0, 126])
	wiper_val = (int(cmd[1]) << 7 or 1 << 6) or (int(cmd[3]) << 5 + 1 << 7)
	l[3] = wiper_val % 127
	l[2] = wiper_val / 128
	return l


def error(cmd):
	return ""


def parseFeedback(line):
	# TODO Publish carDriveMsg as feedback!
	line = line[1:]
	vehicle_spd = ord(line[8]) - 15
	em_flag = ord(line[9])
	throttle_per = ord(line[5]) >> 1
	brake_per = ord(line[4]) >> 1
	steer_angle = ord(line[3]) >> 2
	if (ord(line[3]) and 3) == 1:
		steer_angle *= -1

	twist = Twist()
	twist.linear.x = vehicle_spd / 3.6
	twist.linear.y = twist.linear.z = 0
	twist.angular.x = twist.angular.y = 0
	twist.angular.z = steer_angle * 3.14159265 / 180.0
	return twist


function_dict = {'g': gear_parse, 'i': indicator_parse, 's': steering_parse, 'b': braking_parse, 't': throttle_parse,
				 'w': wiperHorn_parse}

rospy.Subscriber("drive", carDriveMsg, cmd_callback)
pub = rospy.Publisher('feedback', Twist, queue_size=100)


# rospy.Subscriber("cmd_vel", Twist, cmd_callback)

def parse_cmd(command):
	if command:
		parse = function_dict.get(command[0], error)
		return parse(command)
	else:
		return ""


rate = rospy.Rate(readFrequency)
while not rospy.is_shutdown():
	if ser.inWaiting():
		try:
			# print "incoming : ",ser.read(ser.inWaiting()).split()[2]," command : ",command
			# print "incoming : ",ser.read(ser.inWaiting())," command : ",command,"\n"
			line = ser.readline()
			# line=ser.read(14)
			# print "line:" ,line
			global a
			# print line.split()[7]
			a = parseFeedback(line)
			# print "Vehicle speed : " , a[0],'\n'
			# print "Steering angle : ",a[1],'\n'
			# print "emergency status",a[2],'\n'
			# print "throttle percentage : ",a[3],'\n'
			# print "brake percentage : ",a[4],'\n'
			pub.publish(a)
		# print "Parsed Command : ",parse_cmd(command)
		except:

			pass
	rate.sleep()
