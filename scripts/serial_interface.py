#!/usr/bin/env python
import serial
import time
import rospy
from std_msgs.msg import String,Float64,Int32	
from geometry_msgs.msg import Twist, Vector3, Point, Quaternion
from original_logitech.msg._carDriveMsg import carDriveMsg

rospy.init_node('eve_serial_comm', anonymous=True, log_level=rospy.WARN)
#import os
#print os.environ['PYTHONPATH']
#quit()
readFrequency = 150

#dict={'R':-1,'N':0,'D':1,'B':2}

if rospy.has_param('~serial_port'):
    	serial_port=rospy.get_param('~serial_port')
else:
	serial_port = '/dev/ttyACM0'

if rospy.has_param('~baudrate'):
	baudrate=rospy.get_param('~baudrate')

else:
	baudrate = 115200

if rospy.has_param("~enableLogitech"):
	enableLogitech = rospy.get_param('~enableLogitech') #TODO:

ser = serial.Serial(serial_port,baudrate,timeout = 1)
ser.flushInput()
rate = rospy.Rate(readFrequency)
command = ""
sign = lambda x: (2, 1)[x < 0]
gear_map = {'-1':1,'0':2,'1':4}#'B':16}
indicator_map = {'O':0,'L':8,'R':1,'H':9}

def cmd_callback(msg): 
	accelValue = 't' + str(calibrate(msg.accel))
	brakeValue = 'b' + str(calibrate(msg.brake))
	steerValue = 's' + str((msg.steering*-1*40/32767))
	gearValue = 'g' + str(msg.gear)
	print steerValue
	ser.write(throttle_parse(accelValue))
	ser.write(braking_parse(brakeValue))
	# print steering_parse(steerValue)
	ser.write(steering_parse(steerValue))
	ser.write(gear_parse(gearValue))

def calibrate(data):
	if data>=0:
		return int((65534-(32767+data))*100/65534)
	return int((32767-data)*100/65534)


def gear_parse(cmd):
	l = bytearray([255,103,0,0,254])	
	l[2] = ((gear_map[cmd[1:]]<<3)+4 )
	return l

def indicator_parse(cmd):	
	l = bytearray([255,105,0,0,254])		
	l[3] =  ((indicator_map[cmd[1]]<<4) + 8) or ((int(cmd[3])<<2)+1)
	return l

def steering_parse(cmd):
	l = bytearray([255,115,0,0,254])	
	steer_val = int(cmd[1:])
	l[3] = (abs(steer_val)<<2) + sign(steer_val) 
	l[2] = (3<<6)
	return l

def braking_parse(cmd):
	l = bytearray([255,98,0,0,254])	
	brake_val = int(cmd[1:])
	l[3] = (brake_val<<1) +1
	return l
def throttle_parse(cmd):
	l = bytearray([255,116,0,0,254])	
	throttle_val = int(cmd[1:])
	l[3] = (throttle_val<<1) + 1
	return l

def wiperHorn_parse(cmd):
	l = bytearray([255,119,0,0,254])	
	l[3] = (int(cmd[1])<<7 or 1<<6) or (int(cmd[3])<<5 + 1<<7)
	return l

def error(cmd):	
	return ""

def parseFeedback(line):
	line=line[1:]
	vehicle_spd = ord(line[8])-15
	em_flag=ord(line[9])
	throttle_per=ord(line[5])>>1
	brake_per=ord(line[4])>>1
	steer_angle = ord(line[3])>>2
	if (ord(line[3]) and 3 ) == 1:
		steer_angle*=-1

	return vehicle_spd,steer_angle,em_flag,throttle_per,brake_per
  		

function_dict = {'g':gear_parse,'i':indicator_parse,'s':steering_parse,'b':braking_parse,'t':throttle_parse,'w':wiperHorn_parse}

rospy.Subscriber("/drive", carDriveMsg, cmd_callback)
pub = rospy.Publisher('feedback', Float64 , queue_size=100)

# rospy.Subscriber("cmd_vel", Twist, cmd_callback)

def parse_cmd(command):
	if command:		
		parse = function_dict.get(command[0],error)
		return parse(command)
	else:
		return ""
rate = rospy.Rate(readFrequency)
while not rospy.is_shutdown():
	if ser.inWaiting():
		try:	
			#print "incoming : ",ser.read(ser.inWaiting()).split()[2]," command : ",command
			#print "incoming : ",ser.read(ser.inWaiting())," command : ",command,"\n"
			line = ser.readline()
			#line=ser.read(14)
			#print "line:" ,line
			global a
			#print line.split()[7]
			a = parseFeedback(line)
			#print "Vehicle speed : " , a[0],'\n'
			#print "Steering angle : ",a[1],'\n'
			#print "emergency status",a[2],'\n'
			#print "throttle percentage : ",a[3],'\n'
			#print "brake percentage : ",a[4],'\n'
			pub.publish(a[0])
			#print "Parsed Command : ",parse_cmd(command)		
		except:
                        
			pass
	rate.sleep()
