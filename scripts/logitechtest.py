import serial
import time
import rospy
from std_msgs.msg import String,Float64,Int32
from geometry_msgs.msg import Twist, Vector3, Point, Quaternion


rospy.init_node('eve_serial_comm', anonymous=True, log_level=rospy.WARN)
readFrequency = 150

state=0 

dict={'R':-1,'N':0,'D':1,'B':2}

if rospy.has_param('~serial_port'):
	serial_port=rospy.get_param('~serial_port')
else:
	serial_port = '/dev/ttyACM0'
if rospy.has_param('~baudrate'):
	baudrate=rospy.get_param('~baudrate')
else:
	#raise ValueError('The baudrate is not specified as a private ROS parameter. (Eg: 57600)')
	baudrate = 115200
ser = serial.Serial(serial_port,baudrate,timeout = 1)
ser.flushInput()
rate = rospy.Rate(readFrequency)
#command = ""

def cmd_callback_steer(msg):
	msg.data=msg.data*40/32767
	command="s"+str(msg.data)
	ser.write(command)

def cmd_callback_brake(msg):
	command="b"+str(msg.data)
	ser.write(command)

def cmd_callback_accel(msg):
	command="t"+str(msg.data)
	ser.write(command)

def gear_parse(cmd):
	global state
	command="g"
	if(cmd.data==1):
		if(state<=2):
			state=state+1
	elif(cmd.data==-1):
		if(state>=-1):
			state=state-1
	command=command+str(dict[state])
	ser.write(command)


rospy.Subscriber("steering",Int32, cmd_callback_steer)
rospy.Subscriber("brake", Int32, cmd_callback_brake)
rospy.Subscriber("accel", Int32, cmd_callback_accel)
rospy.Subscriber("gear", Int32,gear_parse)

rospy.spin()
